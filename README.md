
# Plymouth-Mageia-themes

A collection the Plymouth themes specifically for/from Mageia

# Install

Copy the files to /usr/share/plymouth/themes/

Run as root:
    # plymouth-set-default-theme -R theme-name

Reboot and see how it looks.

# License

GPL and CC BY-SA 3.0

# Sources

Mageia 6 Default Theme:
mageia-theme-6.4-1.mga6.noarch.rpm

Mageia 5 Default Theme:
mageia-theme-Default-1.5.0.44-1.mga5.noarch.rpm

Mageia 4 Default Theme:
mageia-theme-Default-1.5.0.37-2.mga4.noarch.rpm

Mageia 3 Default Theme:
mageia-theme-Default-1.5.0.35-1.mga3.noarch.rpm

Mageia Coldfire, based on Mageia 5 (2015-07-28):
https://www.gnome-look.org/p/1000019/

Mageia Minimal (2013-02-08):
https://www.gnome-look.org/p/1000037/
